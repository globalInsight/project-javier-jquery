BlobApp = {}; // the namespace

BlobApp.Blobs = (function ($) {

      
    var pub = {};

    var blobslist = {};

    pub.init = function () {     
        
     pub.loadBlobList();  
    }

    pub.loadBlobList = function(){

         $.ajax({
            type: "GET",
            url: "/blobs/blobsList",    
            dataType: "json"
          })
          .done(function( datas ) {  
           
              blobslist = datas;

               var html = "";
               var blob;
               

               $(".main-container-blobs tbody").html("");

               for(var i=0; i < datas.length; i++){

                    blob = datas[i];

                    console.log(blob._id);

                    html = 
                            "<table border = 2px class='container table-bordered table'>" +
                            "<tr>" +
                            "<td>" + blob.name + "</td>" + 
                            "<td>" + blob.badge + "</td>" +
                            "<td>" + blob.isloved + "</td>"+
                            "<td>" + blob.dob + "</td>"+
                            "<td> <a onclick='blobs.editForm(&quot;"+blob._id+"&quot;)' > edit</a></td>"+
                            "<td> <a onclick='blobs.delete(&quot;"+blob._id+"&quot;)' > delete</a></td>"+
                            
                         "</tr>"
                         "</table>"; 


                     $("#myList").append(html); 

                    //console.log(blob);
               }
              
          });
    }



    pub.editForm = function(id){


        var blobSelected = pub.getListById(id);

         $("#inputName").val(blobSelected.name);
         $("#inputBadge").val(blobSelected.badge);
         $("#inputIsLoved").prop("checked", blobSelected.isloved);
         $("#inputDob").val(blobSelected.dob);

         
         $("#save").attr("onclick","blobs.edit('"+id+"')")

    }


        pub.getFormattedDate = function(date) {
              var year = date.getFullYear();
              var month = (1 + date.getMonth()).toString();
              month = month.length > 1 ? month : '0' + month;
              var day = date.getDate().toString();
              day = day.length > 1 ? day : '0' + day;

              return year + '-' + month + '-' + day;
            }


    


    pub.edit = function (id){


         
         var blob = {};
         blob.name = $("#inputName").val();
         blob.badge = $("#inputBadge").val();
         blob.isloved = $("#inputIsLoved").is(":checked");
         blob.dob = $("#inputDob").val();
         blob._id = id;

         blob._method = "PUT";

          $.ajax({
            type: "POST",
            url: "/blobs/"+blob._id+"/edit",        
            data    : blob, //forms user object
          })
          .done(function( datas ) {

           pub.loadBlobList();
           $("#oculto").hide();
           //$.fancybox.close();
                 console.log("the datas has been edited ")
              //console.log(blob._id);
          }).fail(function() {
           
          });

    } 
      pub.getListById = function (id){

        var blob;

        for (var i = 0; i < blobslist.length; i++){            

            if(id == blobslist[i]._id){

                blob = blobslist[i];               
                
            }
        } 

        return blob; 
    }


    pub.create = function (){ 


         var blob = {};
         blob.name = $("#inputName").val();
         blob.badge = $("#inputBadge").val();
         blob.isloved = $("#inputIsLoved").val();
         blob.dob = $("#inputDob").val();
         blob._id = $("#inputId").val();      

        $.ajax({
        type: "POST",
        url: "/blobs",        
        data    : blob, //forms user object
        })
        .done(function( datas ) {

          
        }).fail(function() {
         alert( "error" );
        });
    }



    pub.delete = function(id){


      //alert( "error" );
      $.ajax({
      type: "POST",
      url: "/blobs/"+id+"/edit",    
      dataType: "json",
      data    : {'_method':"Delete"}, //forms user object
      })
      .done(function( datas ) {  

        location.reload();

      //$(".main-container-blobs tbody #blob-"+blobs._id).remove();

        
      }).fail(function() {
      console.log("esta eliminando");
      });
    }



return pub; 





})(jQuery);

// init our modules
jQuery(BlobApp.Blobs.init);

// local refs to the modules
var blobs = BlobApp.Blobs;